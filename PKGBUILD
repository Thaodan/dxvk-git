# Maintainer: Adrià Cereto i Massagué <ssorgatem at gmail.com>
# Contributor: Thaodan <theodorstormgrade at gmail.com>
_srcname=dxvk
pkgbase=${_srcname}-git
pkgname=( mingw-w64-$pkgbase )
pkgver=20180208.8f134ba
pkgrel=1
arch=('any')
_pkgdesc="A Vulkan-based compatibility layer for Direct3D 11 which allows running 3D applications on Linux using Wine."
pkgdesc="$_pkgdesc"
url="https://github.com/doitsujin/dxvk"
license=('zlib/libpng')
groups=()
depends=('vulkan-icd-loader' 'wine-staging')
makedepends=('ninja' 'meson' 'glslang' 'mingw-w64-gcc')
options=('!strip' '!buildflags' 'staticlibs')
source=("git+https://github.com/doitsujin/dxvk.git")
md5sums=("SKIP")

_win32=y # unset this to disabled win32 build

if [ "$_win32" ] ; then
    pkgname+=(mingw-w32-$pkgbase )
fi

pkgver() {
        cd "$_srcname"
        git log -1 --format=%cd.%h --date=short|tr -d -
}

prepare()
{
  cd "$_srcname"
  mkdir -p build.w64
  meson --cross-file build-win64.txt build.w64
  
  if [ "$_win32" ] ; then
      mkdir -p build.w32
      meson --cross-file build-win32.txt build.w32
      (
        cd build.w32
        meson configure \
              -Dprefix=/usr/x86_64-w64-mingw32 \
              -Dbuildtype=release
      )
  fi

  cd build.w64
  meson configure \
        -Dprefix=/usr/x86_64-w64-mingw32 \
        -Dbuildtype=release
}


build() {
  cd "$_srcname"

  if [ "_win32" ] ; then
      (
        cd build.w32
        ninja
      )
  fi
  
  cd build.w64
  ninja
}

if [ "$_win32" ] ; then
    package_mingw-w32-dxvk-git() {
      pkggdesc="$_pkgdesc win32"
      
      cd "$_srcname"/build.w32
      DESTDIR="$pkgdir/" ninja install
    }
fi

package_mingw-w64-dxvk-git()
{
  pkggdesc="$_pkgdesc win64"
  
  cd "$_srcname"/build.w64
  DESTDIR="$pkgdir/" ninja install
}
